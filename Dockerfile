# Set the base image for subsequent instructions
FROM php:7.3.19

# Update packages
RUN apt-get update

# Install PHP and composer dependencies
RUN apt-get install -qq git curl libjpeg-dev libpng-dev libfreetype6-dev libbz2-dev libgmp-dev
RUN apt-get install -y zlib1g-dev libicu-dev g++ libxml2-dev
RUN apt-get install -y zip libzip-dev openssh-client

# Clear out the local repository of retrieved package files
RUN apt-get clean

RUN docker-php-ext-configure zip --with-libzip
RUN docker-php-ext-configure intl

# Install needed extensions
# Here you can install any other extension that you need during the test and deployment process
RUN docker-php-ext-install zip pdo_mysql mbstring json bcmath gd gmp intl xml

# Install Composer
RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Laravel Envoy
RUN composer global require laravel/envoy

# Install PHPCS
RUN composer global require "squizlabs/php_codesniffer=*"
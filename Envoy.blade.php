@servers(['web' => 'deployer@<PRODUCTION_SERVER_IP>'])

@setup
    $repository = 'git@gitlab.com:<USERNAME>/<APP_NAME>.git';
    $releases_dir = '/var/www/app.com.envoy/releases';
    $app_dir = '/var/www/app.com.envoy';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir . '/' . $release;
    $releases_to_keep = 5;
@endsetup

@story('deploy')
    clone_repository
    setup
    build
    update_symlinks
    migrate_database
    optimize
    activate_release
    clean_old_releases
@endstory

@task('setup')
    echo 'Setting Up'
    [ -f {{ $app_dir }}/.env ] || cp {{ $new_release_dir }}/.env.example {{ $app_dir }}/.env
@endtask

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask

@task('build')
    echo "Building ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install --optimize-autoloader --no-dev
    npm install
    npm run production
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    [ -d {{ $app_dir }}/storage ] || cp -R {{ $new_release_dir }}/storage {{ $app_dir }}/storage && chmod 775 {{ $app_dir }}/storage
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env
@endtask

@task('migrate_database')
    cd {{ $new_release_dir }}
    php artisan migrate --force
@endtask

@task('optimize')
    cd {{ $new_release_dir }}
    php artisan config:cache
    php artisan route:cache
    php artisan view:cache
@endtask

@task('activate_release')
    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
    echo 'Restarting php-fpm'
    sudo service php-fpm restart
@endtask

@task('clean_old_releases')
    # This lists our releases by modification time and delete all but the 3 most recent.
    purging=$(ls -dt {{ $releases_dir }}/* | tail -n +{{ $releases_to_keep }});

    if [ "$purging" != "" ]; then
        echo Purging old releases: $purging;
        rm -rf $purging;
    else
        echo "No releases found for purging at this time";
    fi
@endtask

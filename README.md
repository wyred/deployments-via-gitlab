# Deployments via gitlab

Automating deployments of my laravel applications using gitlab.

## Disclaimer

A lot of what I know is self-taught and so some terms may have been used wrongly. I welcome corrections.

I'm using Amazon Linux as the OS for my production servers as I use AWS EC2 for my servers.

## Overview

1. Commit changes and push to gitlab
2. gitlab uses a docker image to run tests
3. If tests passes, the deploy process starts using the same image
4. An SSH key stored with gitlab is passed in to the docker image and Laravel Envoy is used to deploy the application onto the server

## Files in this repo:

- Dockerfile  
the file used to build a docker image for running tasks on gitlab
- gitlab-ci.yml  
the file that defines the tasks that gitlab runs on the docker image
- Envoy.blade.php  
the file used by Laravel Envoy for deployment
- .env.testing  
an .env file for the testing environment
- deployer  
a file to be placed inside /etc/sudoers.d so that deployer can restart php-fpm

## Sources
A lot of this guide is taken from the following sources:
- https://docs.gitlab.com/ee/ci/examples/laravel_with_gitlab_and_envoy/
- https://docs.gitlab.com/ee/ssh/README.html
- https://gist.github.com/michaeldyrynda/31e98d51180c99e44091

## Things to install
- Docker

## Section 1 - Building the docker image
1. Copy `Dockerfile` to the app directory
2. In terminal, cd into the app directory
3. Login to gitlab's docker registry:  
`docker login registry.gitlab.com`
4. Build the docker image:  
`docker build -t registry.gitlab.com/<USERNAME>/<APP_NAME> .`
5. Push the docker image to gitlab  
`docker push registry.gitlab.com/<USERNAME>/<APP_NAME>`

## Section 2 - GitLab CI
1. Copy `.gitlab-ci.yml` to your app directory and edit this file
2. line 4 needs to be updated to the same destination URL that your docker image was pushed to in Section 1 Step 5
3. If you do not need phpcs code style checks, comment line 42
4. Comment out lines 47-50 if you do not need security checks on the installed composer packages

## Section 3 - Server setup
1. Create a user on your server to be used for this deployment  
`sudo adduser deployer --disabled-password`
2. Give this user full permissions to the directory the app will be deployed into  
`sudo setfacl -R -m u:deployer:rwx /var/www/app.com.envoy`
3. Switch to this user  
`sudo su deployer`
4. Generate SSH keys for this user   
`ssh-keygen -t rsa -b 2048 -C "deployer@app.com"`
5. Copy the public key into `authorized_keys` file to enable SSH login  
`cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys`
6. Set proper permissions for the `authorized_keys` file otherwise SSH login will not work  
`chmod 600 .ssh/authorized_keys`
7. Copy the contents of the private key file into gitlab. App's repo -> Settings -> CI/CD -> Variables  
![](guide_images/gitlab-cicd-variable.png)
8. Copy the contents of the public key file into gitlab. App's repo -> Settings -> Repository -> Deploy Keys. Write access is not required.
9. Test the connection to gitlab so that gitlab's servers are added to the `known_hosts` file. You can remove the cloned directory after it is finished  
`git clone git@gitlab.example.com:<USERNAME>/<APP_NAME>.git`
10. Setup your nginx config file. I will not go into detail on this step. Just ensure that the root folder specified is `/var/www/app.com.envoy/current`
11. In the app directory, setup the .env file for your laravel application 

I append `.envoy` to the app's folder name to remind myself that this app is deployed using Envoy.

## Section 4 - restarting php-fpm
php-fpm needs to be restarted after every deployment or you may not see the new changes. Copy the `deployer` file into `/etc/sudoers.d` directory.  
This will allow the deployer user to restart php-fpm.

## Section 5 - Envoy
1. Copy `Envoy.blade.php` into your app directory
2. You'll only need to update line 1, 4-6

Quick explanation on the steps defined in this Envoy file

- setup  
Checks if an .env file exists in the app_dir, if not it will copy the .env.example from the latest release

- clone_repository  
Begins cloning the latest version of your app into the server

- build  
Install composer dependencies. Install npm packages and prepares them for production use

- update_symlinks  
If a storage folder does not exist, copy it from the latest release.  
symlink the .env file and storage folder in the release folder to the one in the app folder. This ensures that these 2 files/folders persist across deployments.

- migrate_database  
run database migrations

- optimize  
run laravel optimizations such as caching the config, routes and view files

- activate_release  
symlink the `current` folder inside the app directory to the newly deployed release folder

- clean_old_releases  
As each deployment creates a new release folder, this ensures that we don't keep too many old releases and thus filling up the storage space.
You can customize the number of release folders kept by editing line 9 of the `Envoy.blade.php` file

## Section 5 - Commit

After all the setup is done, everytime you commit your changes, gitlab will run the tests and deploy the latest changes to your server.